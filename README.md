# Docker Swarm and Spring REST application update with no downtime #

## Prerequisities ##

* Two nodes in Microsoft Azure Cloud (172.20.122.4 and 172.20.122.7)
* This repository git cloned at both nodes
* In this repo contained Spring REST application in two versions with builded Docker images at both nodes 
(I won't be using registry).

Version 1:
```
@RestController
public class TestController {
	
	@RequestMapping("/invokeTest")
	public String invokeController() {
		return "Invoked controller method. Version 1";
	}
}
```

Version 2:
```
@RestController
public class TestController {
	
	@RequestMapping("/invokeTest")
	public String invokeController() {
		return "Invoked controller method. Version 2";
	}
}
```

## Building images of both versions ##

Run "**mvn clean install**" first to build the app. Then...
```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker build -t demodocker:1 .
Sending build context to Docker daemon 33.14 MB
Step 1/3 : FROM tomcat:8-jre8
 ---> 8c90bc7187ec
Step 2/3 : MAINTAINER "Tomas Kloucek <kloucektomas@yahoo.com">
 ---> Running in e739a6606283
 ---> 619082ad4169
Removing intermediate container e739a6606283
Step 3/3 : ADD ./target/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
 ---> c6bced634b20
Removing intermediate container 2f8c6b4199da
Successfully built c6bced634b20

cmwork1@cmwork1docker:~/docker-spring-mvc$ docker build -t demodocker:2 .
Sending build context to Docker daemon 33.15 MB
Step 1/3 : FROM tomcat:8-jre8
 ---> 8c90bc7187ec
Step 2/3 : MAINTAINER "Tomas Kloucek <kloucektomas@yahoo.com">
 ---> Using cache
 ---> 619082ad4169
Step 3/3 : ADD ./target/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
 ---> c0f0d1a9de6d
Removing intermediate container 959a57102304
Successfully built c0f0d1a9de6d
cmwork1@cmwork1docker:~/docker-spring-mvc$
```

Perform this at the second nodes as well.
At both nodes you should have:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
demodocker          2                   c0f0d1a9de6d        22 seconds ago      379 MB
demodocker          1                   c6bced634b20        10 minutes ago      379 MB
tomcat              8-jre8              8c90bc7187ec        2 days ago          365 MB
```

In real case you'd be working with your docker registry. So normally you'd just need to push 
your versioned Docker image into registry once and your docker swarm nodes will just need to
access this registry.  

## Creating the Docker Swarm Cloud ##

Node1 is going to be the leader:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker swarm init
Swarm initialized: current node (spdr4qmn8o984b3fucbnjs1tc) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-2jlnkuzifankt4yzr38k8bnvk13s10svtef01h1katoe6ntlvo-38x6sm06k4xi3433bfu552o2x \
    172.20.122.4:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Lets join Node2 to the leader node:

```
cmwork2@cmwork2docker:~/docker-spring-mvc$ docker swarm join \
     --token SWMTKN-1-2jlnkuzifankt4yzr38k8bnvk13s10svtef01h1katoe6ntlvo-38x6sm06k4xi3433bfu552o2x \
     172.20.122.4:2377
This node joined a swarm as a worker.
```

Verify that nodes are connected:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker node ls
ID                           HOSTNAME       STATUS  AVAILABILITY  MANAGER STATUS
m8u12bu18ogngxr54owyuuhqz    cmwork2docker  Ready   Active
spdr4qmn8o984b3fucbnjs1tc *  cmwork1docker  Ready   Active        Leader
```

Since this is multinode cluster, we will need overlay network to put the nodes to:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker network create -d overlay spring-cloud-network
y8goxpdjcxnk9891t0zp33nxe
```

Now lets deploy version 1 to Docker Swarm Cloud:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker service create --name demodocker --network spring-cloud-network --publish 8080:8080 --replicas 2 --update-delay 10s demodocker:1
unable to pin image demodocker:1 to digest: errors:
denied: requested access to the resource is denied
unauthorized: authentication required

ys79rqr9s6hkl5oq6xe8c9kw0
```
I don't use any registry and Docker 1.13 tries to verify the image hash with non-existing default registry. But you can ignore this warning.
Will be suppressible in the next Docker releases.
Lets verify that service is running:

```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker service ps demodocker
ID            NAME          IMAGE         NODE           DESIRED STATE  CURRENT STATE           ERROR  PORTS
9ye0qzhfjnim  demodocker.1  demodocker:1  cmwork2docker  Running        Running 23 seconds ago
m1331q9h3sql  demodocker.2  demodocker:1  cmwork1docker  Running        Running 24 seconds ago
cmwork1@cmwork1docker:~/docker-spring-mvc$
```

## Testing the deployed Docker service (version 1) ##

Simply hit in the browser (you can hit node2 as well) :

```
http://172.20.122.4:8080/demo-0.0.1-SNAPSHOT/invokeTest
Text output should be: Invoked controller method. Version 1
```

## Updating the service to version 2 ##
(again ignore the warning since we don't use registry)
```
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker service update --image demodocker:2 demodocker
unable to pin image demodocker:2 to digest: errors:
denied: requested access to the resource is denied
unauthorized: authentication required

demodocker
cmwork1@cmwork1docker:~/docker-spring-mvc$ docker service ps demodocker
```

We set the update delay to 10s so the replicas will be updated in the 10seconds intervals without the whole service downtime. 
For more info check perfect documentation

```
https://docs.docker.com/engine/reference/commandline/service_update/
```

New browser hit should output this:

```
http://172.20.122.4:8080/demo-0.0.1-SNAPSHOT/invokeTest
Text output should be: Invoked controller method. Version 2
```


regards

T.